package com.study.demo.pojo;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @ClassName: User
 * @Description:
 * @author: limingxing
 * @Date: 2019-11-22 17:27
 */
@Data
@Table(name = "user")
public class User {
    private Integer id;
    private String loginName;
    private String password;
    private String email;

    private Date createTime;


}
