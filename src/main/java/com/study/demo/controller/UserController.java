package com.study.demo.controller;

import com.study.demo.common.result.ResponseData;
import com.study.demo.common.result.ResponseUtils;
import com.study.demo.common.result.ResultData;
import com.study.demo.constants.ResultCodeConstants;
import com.study.demo.dto.*;
import com.study.demo.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName: UserController
 * @Description:
 * @author: limingxing
 * @Date: 2019-11-22 19:49
 */
@RestController
@RequestMapping("/user")
@Api(value = "用户模块", tags = "用户操作模块")
public class UserController {

    @Autowired
    private IUserService userService;

    @PostMapping("/add")
    @ApiOperation(value = "添加用户", notes = "根据参数添加用户")
    public ResponseData<AddUserResponse> add(@RequestBody AddUserRequest request){
        AddUserResponse response = userService.add(request);
        if (response.getCode().equals(ResultCodeConstants.SUCCESS.getCode())) {
            return new ResponseUtils().setData(response);
        }
        return new ResponseUtils().setErrorMsg(response.getMsg());
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(value = "删除用户", notes = "根据用户ID删除用户")
    public ResponseData<DeleteUserResponse> del(DeleteUserRequest request){
        DeleteUserResponse response = userService.deleteById(request);
        if (response.getCode().equals(ResultCodeConstants.SUCCESS.getCode())) {
            return new ResponseUtils().setData(response);
        }
        return new ResponseUtils().setErrorMsg(response.getMsg());
    }

    @PutMapping("/updateById")
    @ApiOperation(value = "更新用户", notes = "根据参数更新用户")
    public ResponseData<UpdateUserResponse> update(UpdateUserRequest request){
        UpdateUserResponse response = userService.updateById(request);
        if (response.getCode().equals(ResultCodeConstants.SUCCESS.getCode())) {
            return new ResponseUtils().setData(response);
        }
        return new ResponseUtils().setErrorMsg(response.getMsg());
    }

    @GetMapping("/list")
    @ApiOperation(value = "用户列表", notes = "根据参数查询用户列表")
    public List getList(){
        return userService.getList();
    }


    @GetMapping("/getUserList")
    @ApiOperation(value = "用户列表", notes = "根据参数查询用户列表")
    public ResultData getUserList(){
        ResultData resultData = userService.getUserList();
        return resultData;
    }
}
