package com.study.demo.dto;

import com.study.demo.common.result.AbstractRequest;
import com.study.demo.constants.ResultCodeConstants;
import com.study.demo.exception.ValidateException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName: AddUserRequest
 * @Description:
 * @author: limingxing
 * @Date: 2019-11-23 10:06
 */
@Data
@ApiModel(value = "删除用户请求参数")
public class DeleteUserRequest extends AbstractRequest {
    @ApiModelProperty(value = "用户ID",required = true)
    private Integer userId;

    @Override
    public void requestCheck() {
        if(null == userId){
            throw new ValidateException(
                    ResultCodeConstants.REQUEST_CHECK_FAILURE.getCode(),
                    ResultCodeConstants.REQUEST_CHECK_FAILURE.getMessage());
        }
    }
}
