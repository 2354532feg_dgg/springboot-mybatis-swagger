package com.study.demo.dto;

import com.study.demo.common.result.AbstractResponse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName: AddUserRequest
 * @Description:
 * @author: limingxing
 * @Date: 2019-11-23 10:06
 */
@ApiModel(value = "添加用户响应数据")
public class AddUserResponse extends AbstractResponse {

}
