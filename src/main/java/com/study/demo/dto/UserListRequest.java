package com.study.demo.dto;

import com.study.demo.common.result.AbstractRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName: AddUserRequest
 * @Description:
 * @author: limingxing
 * @Date: 2019-11-23 10:06
 */
@Data
@ApiModel(value = "查询用户列表请求参数")
public class UserListRequest extends AbstractRequest {
    @ApiModelProperty(value = "登录名")
    private String loginName;
    @ApiModelProperty(value = "邮箱")
    private String email;

    @Override
    public void requestCheck() {

    }
}
