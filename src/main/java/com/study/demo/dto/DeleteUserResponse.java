package com.study.demo.dto;

import com.study.demo.common.result.AbstractResponse;
import io.swagger.annotations.ApiModel;

/**
 * @ClassName: AddUserRequest
 * @Description:
 * @author: limingxing
 * @Date: 2019-11-23 10:06
 */
@ApiModel(value = "删除用户响应数据")
public class DeleteUserResponse extends AbstractResponse {

}
