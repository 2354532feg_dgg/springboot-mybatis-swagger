package com.study.demo.dto;

import com.study.demo.common.result.AbstractRequest;
import com.study.demo.constants.ResultCodeConstants;
import com.study.demo.exception.ValidateException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @ClassName: AddUserRequest
 * @Description:
 * @author: limingxing
 * @Date: 2019-11-23 10:06
 */
@Data
@ApiModel(value = "添加用户请求参数")
public class AddUserRequest extends AbstractRequest {
    @ApiModelProperty(value = "登录名",required = true)
    private String loginName;
    @ApiModelProperty(value = "登录密码")
    private String password;
    @ApiModelProperty(value = "邮箱")
    private String email;

    @Override
    public void requestCheck() {
        if(StringUtils.isBlank(loginName)||StringUtils.isBlank(password)){
            throw new ValidateException(
                    ResultCodeConstants.REQUEST_CHECK_FAILURE.getCode(),
                    ResultCodeConstants.REQUEST_CHECK_FAILURE.getMessage());
        }
    }
}
