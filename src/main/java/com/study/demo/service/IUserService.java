package com.study.demo.service;

import com.study.demo.common.result.ResultData;
import com.study.demo.dto.*;
import com.study.demo.pojo.User;

import java.util.List;

/**
 * @ClassName: IUserService
 * @Description:
 * @author: limingxing
 * @Date: 2019-11-22 19:54
 */
public interface IUserService {

    /**
     * 添加用户
     * @param request
     * @return
     */
    AddUserResponse add(AddUserRequest request);

    /**
     * 删除用户
     * @param request
     * @return
     */
    DeleteUserResponse deleteById(DeleteUserRequest request);

    /**
     * 修改用户
     * @param request
     * @return
     */
    UpdateUserResponse updateById(UpdateUserRequest request);

    List getList();

    ResultData getUserList();
}
