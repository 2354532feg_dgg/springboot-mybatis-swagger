package com.study.demo.service.impl;

import com.study.demo.common.result.ResultData;
import com.study.demo.constants.ResultCodeConstants;
import com.study.demo.dto.*;
import com.study.demo.exception.ExceptionProcessorUtils;
import com.study.demo.mapper.UserMapper;
import com.study.demo.pojo.User;
import com.study.demo.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: UserServiceImpl
 * @Description:
 * @author: limingxing
 * @Date: 2019-11-22 19:55
 */
@Slf4j
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public AddUserResponse add(AddUserRequest request) {
        log.info("Begin UserServiceImpl.add request: [{}]", request);
        AddUserResponse response = new AddUserResponse();
        try {
            request.requestCheck();
            User user = new User();
            user.setLoginName(request.getLoginName());
            user.setEmail(request.getEmail());
            user.setPassword(request.getPassword());
            user.setCreateTime(new Date());
            Integer result = userMapper.insert(user);
            log.info("UserServiceImpl.add effect result : [{}]", result);
            if (result > 0) {
                response.setCode(ResultCodeConstants.SUCCESS.getCode());
                response.setMsg(ResultCodeConstants.SUCCESS.getMessage());
            } else {
                response.setCode(ResultCodeConstants.REQUEST_DATA_ERROR.getCode());
                response.setMsg(ResultCodeConstants.REQUEST_DATA_ERROR.getMessage());
            }
        } catch (Exception e) {
            log.error("UserServiceImpl.add Occur Exception : {}", e);
            ExceptionProcessorUtils.wrapperHandlerException(response, e);
        }
        return response;
    }

    @Override
    public DeleteUserResponse deleteById(DeleteUserRequest request) {
        log.info("Begin UserServiceImpl.deleteById request: [{}]", request);
        DeleteUserResponse response = new DeleteUserResponse();
        try {
            request.requestCheck();
            Integer result = userMapper.deleteById(request.getUserId());
            log.info("UserServiceImpl.deleteById effect result : [{}]", result);
            if (result > 0) {
                response.setCode(ResultCodeConstants.SUCCESS.getCode());
                response.setMsg(ResultCodeConstants.SUCCESS.getMessage());
            } else {
                response.setCode(ResultCodeConstants.DATA_NOT_EXIST.getCode());
                response.setMsg(ResultCodeConstants.DATA_NOT_EXIST.getMessage());
            }
        } catch (Exception e) {
            log.error("UserServiceImpl.deleteById Occur Exception : {}", e);
            ExceptionProcessorUtils.wrapperHandlerException(response, e);
        }
        return response;
    }

    @Override
    public UpdateUserResponse updateById(UpdateUserRequest request) {
        log.info("Begin UserServiceImpl.updateById request: [{}]", request);
        UpdateUserResponse response = new UpdateUserResponse();
        try {
            request.requestCheck();
            User user = new User();
            user.setId(request.getId());
            user.setPassword(request.getPassword());
            user.setLoginName(request.getLoginName());
            user.setEmail(request.getEmail());
            Integer result = userMapper.updateById(user);
            log.info("UserServiceImpl.updateById effect result : [{}]", result);
            if (result > 0) {
                response.setCode(ResultCodeConstants.SUCCESS.getCode());
                response.setMsg(ResultCodeConstants.SUCCESS.getMessage());
            } else {
                response.setCode(ResultCodeConstants.DATA_NOT_EXIST.getCode());
                response.setMsg(ResultCodeConstants.DATA_NOT_EXIST.getMessage());
            }
        } catch (Exception e) {
            log.error("UserServiceImpl.updateById Occur Exception : {}", e);
            ExceptionProcessorUtils.wrapperHandlerException(response, e);
        }
        return response;
    }

    @Override
    public List getList() {
        return userMapper.selectAll();
    }

    @Override
    public ResultData getUserList() {
        List<User> list = userMapper.selectAll();
        ResultData resultData = ResultData.success(list);
        return resultData;
    }
}
