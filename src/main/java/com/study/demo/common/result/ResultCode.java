package com.study.demo.common.result;

/**
 * @ClassName: SysRetCodeConstants
 * @Description: 统一返回码
 * @author: limingxing
 * @Date: 2019-11-22 21:15
 */
public enum ResultCode {

    /*成功状态码*/
    SUCCESS(200, "成功"),
    /*参数错误1001-1999*/
    PARAM_IS_INVALID(1001, "参数无效"),
    PARAM_IS_BLANK(1002, "参数为空"),
    PARAM_TYPE_BIND_ERROR(1003, "参数类型错误"),
    PARAM_NOT_COMPLETE(1004, "参数缺失"),
    REQUISITE_PARAMETER_NOT_EXIST       (1005, "必要的参数不能为空"),
    PERMISSION_DENIED                   (1091, "权限不足"),
    DB_EXCEPTION                        (1097, "数据库异常"),
    SYSTEM_TIMEOUT                      (1098, "系统超时"),
    DB_SAVE_EXCEPTION                   (1096,"数据保存异常"),

    /*用户错误2001-2999*/
    USER_NOT_LOGGED_IN(2001, "用户未登录"),
    USER_LOGIN_ERROR(2002, "账号不存在或密码错误"),
    USER_ACCOUNT_FORBIDDEN(2003, "账号已被禁用"),
    USER_NOT_EXIST(2004, "用户不存在"),
    USER_HAS_EXISTED(2005, "用户已存在");


    private Integer code;
    private String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * Getter method for property <tt>code</tt>.
     *
     * @return property value of code
     */
    public Integer getCode() {
        return code;
    }


    /**
     * Getter method for property <tt>message</tt>.
     *
     * @return property value of message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setter method for property <tt>message</tt>.
     *
     * @param message value to be assigned to property message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public static String getMessage(Integer code) {
        for (ResultCode s : ResultCode.values()) {
            if (null == code)
                break;
            if (s.code.equals(code)) {
                return s.message;
            }
        }
        return null;
    }
}
