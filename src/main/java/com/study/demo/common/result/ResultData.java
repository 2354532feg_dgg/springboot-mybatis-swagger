package com.study.demo.common.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: ResultData
 * @Description:
 * @author: limingxing
 * @Date: 2019-11-26 10:40
 */
@Data
public class ResultData implements Serializable {

    private Integer code;
    private String message;
    private Object data;

    public ResultData(){}
    public ResultData(ResultCode resultCode){
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }
    public ResultData(ResultCode resultCode,Object data){
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.data = data;
    }

    /**
     * 返回成功
     * @return
     */
    public static ResultData success(){
        ResultData resultData = new ResultData(ResultCode.SUCCESS);
        return resultData;
    }

    /**
     * 返回成功
     * @return
     */
    public static ResultData success(Object data){
        ResultData resultData = new ResultData(ResultCode.SUCCESS);
        resultData.setData(data);
        return resultData;
    }

    /**
     * 返回失败
     * @return
     */
    public static ResultData failure(ResultCode resultCode){
        ResultData resultData = new ResultData(resultCode);
        return resultData;
    }


    /**
     * 返回失败
     * @return
     */
    public static ResultData failure(ResultCode resultCode,Object data){
        ResultData resultData = new ResultData(resultCode);
        resultData.setData(data);
        return resultData;
    }
}
