package com.study.demo.common.result;

import java.io.Serializable;

/**
 * @ClassName: AbstractRequest
 * @Description: 抽象请求封装
 * @author: limingxing
 * @Date: 2019-11-19 15:43
 */
public abstract class AbstractRequest implements Serializable {

    /**
     * 请求校验
     */
    public abstract void requestCheck();

    @Override
    public String toString() {
        return "AbstractRequest{}";
    }
}
