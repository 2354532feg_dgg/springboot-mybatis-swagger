package com.study.demo.common.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @ClassName: AbstractResponse
 * @Description: 抽象公用返回类
 * @author: limingxing
 * @Date: 2019-11-18 16:51
 */
@ApiModel(value = "抽象公用响应参数")
public abstract class AbstractResponse implements Serializable {

    private static final long serialVersionUID = -7505997295595095971L;
    @ApiModelProperty("返回码")
    private String code;
    @ApiModelProperty("返回信息")
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
