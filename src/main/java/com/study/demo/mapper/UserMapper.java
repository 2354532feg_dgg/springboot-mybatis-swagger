package com.study.demo.mapper;

import com.study.demo.pojo.User;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface UserMapper extends Mapper<User>, MySqlMapper<User> {

    int deleteById(Integer userId);

    int updateById(User user);
}
