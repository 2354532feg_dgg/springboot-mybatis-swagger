package com.study.demo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ClassName: ignoreJWT
 * @Description: 在使用该注解的方法上不进行jwt校验
 * @author: limingxing
 * @Date: 2019-11-22 17:27
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value={ElementType.METHOD, ElementType.TYPE})
public @interface IgnoreJWT {
}
